# Include custom standard package
list(PREPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/../Modules)
include(FindPackageStandard)

# Load using standard package finder
find_package_standard(
  NAMES hdf5 hdf5_cpp hdf5_hl hdf5_hl_cpp
  HEADERS "hdf5.h" "H5Cpp.h"
  PATHS /usr/include/hdf5/serial
)
