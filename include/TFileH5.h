/**
 * *********************************************
 *
 * \file TFileH5.h
 * \brief Header of the TFileH5 class
 * \author Marco Meyer \<marco.meyer@omu.ac.jp\>
 *
 * *********************************************
 *
 * \class TFileH5
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \version Revision: 1.0
 * \date May 19th, 2022
 *
 * *********************************************
 */

#ifndef TFileH5_H
#define TFileH5_H

#include <Riostream.h>
#include <TObject.h>
#include <TString.h>
#include <TRandom.h>
#include <TFormula.h>

#include <TParameter.h>

#include <TFile.h>
#include <TTree.h>
#include <TObject.h>
#include <TSystem.h>

#include <exception>

// /!\ SEQUENTIAL HDF5 IS UNTESTED
// I didn't have sequential files..
// So please.. send me an email (marco.meyer@cern.ch) with a sample if this breaks..

class TFileH5: public TObject
{
        private:
                class Impl;
                Impl *pImpl = NULL;

        public:

                TFileH5(const char *fname, Option_t *option = "", Bool_t _sequential = false, int _verbosity = 0);
                ~TFileH5();

                static TFileH5 *Open(const char *fname, Option_t *option = "", Bool_t sequential = false, int verbosity = 0) { return new TFileH5(fname, option, sequential, verbosity); }

                TObject *Read(TString path);

                void GetListOfKeys();
                TString GetName();

                void Print(Option_t *option = "");
                void Save(Option_t *option = "CREATE", TString = "");
                void Close(Option_t *option="");

        ClassDef(TFileH5,1);
};

#endif
